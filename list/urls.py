from django.urls import path
from list import views
from django.conf.urls.static import static # 追加
from django.conf import settings # 追加

app_name = 'list'
urlpatterns = [
    # 一覧
    path('list/', views.list, name='list'),
    path('detail/', views.detail, name='detail'),
    path('delete/', views.delete, name = 'delete'),
    path('<int:shop_id>/<int:comment_id>', views.delete_d, name = 'delete_d'),
    path('<int:shop_id>/', views.inf, name = 'inf'),
    ]
if settings.DEBUG:
    urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) # 追加
