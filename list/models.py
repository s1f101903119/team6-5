from django.db import models
from django.core.validators import RegexValidator
from django.utils import timezone

#from list import views #追加モジュール_電話番号、郵便番号に使う


class Shop(models.Model):
    """店舗名"""
    name = models.CharField('店舗名', max_length=255)
    """概要"""
    views = models.TextField('説明')
    """郵便番号"""
    #postal_code_regex 郵便番号は「1234567」の形式で入力する必要があります。最大7桁まで使用できます。
    postal_code_regex = RegexValidator(regex=r'^[0-9]{7}$', message = ("Postal Code must be entered in the format: '1234567'. Up to 7 digits allowed."))
    pos_code = models.CharField(validators=[postal_code_regex], max_length=7, verbose_name='郵便番号')
    """住所"""
    #場所によっては長くなるため
    address = models.CharField('住所', max_length=255)
    """電話番号"""
    #tel_number_regex 電話番号は「09012345678」の形式で入力する必要があります。最大15桁まで使用できます。
    tel_number_regex = RegexValidator(regex=r'^[0-9]{10,11}$', message = ("Tel Number must be entered in the format: '09012345678'. Up to 15 digits allowed."))
    tel_num = models.CharField(validators=[tel_number_regex], max_length=15, verbose_name='電話番号')
    """公式サイトURL"""
    #場所によっては長くなるため
    site_url = models.CharField('site_URL', max_length=255)
    """画像URL"""
    img_url =models.ImageField('img_URL', upload_to = 'images', blank = True, null = True)

"""コロナ対策"""
class Check(models.Model):
    """店のコロナ対策"""
    check_23 = models.BooleanField('check_23', default=False, null=True)
    check_33 = models.BooleanField('check_33', default=False, null=True)
    check_34 = models.BooleanField('check_34', default=False, null=True)
    check_35 = models.BooleanField('check_35', default=False, null=True)
    check_36 = models.BooleanField('check_36', default=False, null=True)
    check_41 = models.BooleanField('check_41', default=False, null=True)
    """お客様へのお願い"""
    check_2 = models.BooleanField('check_2', default=False, null=True)
    check_3 = models.BooleanField('check_3', default=False, null=True)
    check_4 = models.BooleanField('check_4', default=False, null=True)
    check_10 = models.BooleanField('check_10', default=False, null=True)
    check_12 = models.BooleanField('check_12', default=False, null=True)
    check_15 = models.BooleanField('check_15', default=False, null=True)
    check_22 = models.BooleanField('check_22', default=False, null=True)
    check_28 = models.BooleanField('check_28', default=False, null=True)
    check_32 = models.BooleanField('check_32', default=False, null=True)

    """shopとの関係"""
    shop = models.OneToOneField(Shop, related_name="shop_check", on_delete=models.CASCADE, null=True)

class Comment(models.Model):
    """タイトル"""
    title = models.CharField('タイトル', max_length=255)
    """名前"""
    name = models.CharField('名前', max_length=255)
    """コメント"""
    comment = models.TextField('コメント')
    """作成時間"""
    regist_date = models.DateTimeField(default=timezone.now)
    """ショップ"""
    Shop = models.ForeignKey(Shop, on_delete=models.CASCADE)


    def __str__(self):
        return self.name
