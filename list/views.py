from tabnanny import check
from unicodedata import name
from xml.etree.ElementTree import Comment
from django.http.response import Http404
from django.shortcuts import render, redirect
from django.http import HttpResponse
from list.models import Shop
from list.models import Check
from list.models import Comment
from mysite.settings import MEDIA_ROOT
import os
from pathlib import Path
import urllib
from django.db.models import Q
from django.views.decorators.csrf import csrf_protect

@csrf_protect

# 登録、編集
def detail(request):
    if request.method == 'POST':
        shop = Shop(
            name=request.POST['name'],
            pos_code = request.POST['pos_code'],
            address = request.POST['address'],
            tel_num = request.POST['tel_num'],
            site_url = request.POST['URL'],
            img_url = request.FILES['img'],
            views = request.POST['views']
        )
        shop.save()
        check = Check(
            check_23 = request.POST['check_23'],
            check_33 = request.POST['check_33'],
            check_34 = request.POST['check_34'],
            check_35 = request.POST['check_35'],
            check_36 = request.POST['check_36'],
            check_41 = request.POST['check_41'],
            check_2 = request.POST['check_2'],
            check_3 = request.POST['check_3'],
            check_4 = request.POST['check_4'],
            check_10 = request.POST['check_10'],
            check_12 = request.POST['check_12'],
            check_15 = request.POST['check_15'],
            check_22 = request.POST['check_22'],
            check_28 = request.POST['check_28'],
            check_32  = request.POST['check_32'],
            shop = shop
        )
        check.save()

        return redirect('../list')


    return render(request, 'list/detail.html')

#一覧
def list(request):
    q_word = request.GET.get('query')

    if q_word:
        context = {
            'shops': Shop.objects.filter(Q(name__icontains=q_word) | Q(address__icontains=q_word)),
            }
    else:
        context = {
            'shops': Shop.objects.all(),
            }
    
    return render(request, 'list/list.html', context)

#詳細画面
def inf(request, shop_id):
    try:
        shop = Shop.objects.get(pk=shop_id)
    except Shop.DoesNotExist:
        raise Http404("見つからないよ")
    context = {
        'shop' : shop
    }
    if request.method == 'POST':
        comment = Comment(
            title=request.POST['title'],
            name=request.POST['name'],
            comment=request.POST['comment'],
            Shop = shop
        )
        comment.save()
    return render(request, 'list/information.html', context)

#新規と編集
def edit(request, id=None):
    return HttpResponse("編集")

#削除_コメント
def delete_d(request, shop_id, comment_id):
    try:
        comment = Comment.objects.get(pk=comment_id)
    except Comment.DoesNotExist:
        raise Http404("Comment does not exist")
    comment.delete()
    url = '../' + str(shop_id)
    return redirect(url)

#削除_store
def delete(request):
    image_path = []
    try:
        shop = Shop.objects.all()
        check = Check.objects.all()
        for a in shop:
            os.remove(os.path.join(MEDIA_ROOT.__str__(), a.img_url.name))
    except Shop.DoesNotExist:
        raise Http404("Shop does not exist")
    shop.delete()
    check.delete()
    return redirect('../list')
