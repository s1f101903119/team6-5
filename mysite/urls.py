from django.contrib import admin
from django.urls import path, include
from home import views
from list import views
from django.conf.urls.static import static # 追加
from django.conf import settings # 追加

urlpatterns = [
    # path('', views.home, name='home'),
    path('', include('home.urls')),
    path('', include('list.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) # 追加
