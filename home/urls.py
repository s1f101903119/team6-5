from django.urls import path
from home import views

app_name = 'home'
urlpatterns = [
    #トップページ
    path('', views.index, name='index'),

    #サービスについて
    path('serve', views.service, name = 'service'),
    
    path('contact', views.contact, name = 'contact'),
    
]
