from django.shortcuts import render
from django.http import HttpResponse
from list.models import Shop


def index(request):
    context = {
        'shops': Shop.objects.all()
    }
    return render(request,'home/index.html', context)

def service(request):
    return render(request, 'home/service.html')

def contact(request):
    return render(request, 'home/contact.html')
